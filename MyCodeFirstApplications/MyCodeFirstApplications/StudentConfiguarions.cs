﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

namespace MyCodeFirstApplications
{
      public class StudentConfiguarions : EntityTypeConfiguration<Student>
        
    {
        public StudentConfiguarions()
        {
            this.Property(s => s.StudenName)
                .IsRequired()
                .HasMaxLength(50);

            this.HasOptional(s => s.Address)
             .WithRequired(ad => ad.Student);

            this.Property(s => s.StudenName)
                .IsConcurrencyToken();
                
        }
    }
}
