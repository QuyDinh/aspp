﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCodeFirstApplications
{
    public class Student
    {
        public int StudentId { get; set; }
        public string StudenName { get; set; }
        public DateTime? DataOfBirth { get; set; }
        public decimal Height { get; set; }
        public float Weight { get; set; }
        public int? GradeId { get; set; }
        public virtual Grade Grade{get; set;}
        public virtual StudentAddress Address { get; set; }

        public virtual ICollection<Course> Courses { get; set; }
    }
}
